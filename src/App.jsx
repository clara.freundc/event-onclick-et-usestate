import { useState } from 'react'
import './App.css'

function App() {

  //compteur

  //je définie une variable, le compteur, et je lui donne un useState qui va créer une autre variable count, lié à la fonction setCount qui permet de 
  //modifier la variable précédente. useState est initialisé à 0, ça veut dire qu'il vaut 0 dans son état de base.
  const [count, setCount] = useState(0);

  //on créer les fonctions nécessaires pour les comportements des boutons
  //Celle là incrémente le nombre de partcipants de 1
  function plus () {
    setCount(count+1);
  }
  //Celle ci décrémente le nombre de partcipants de 1, tout en empêchant le compteur de descendre plus bas
  //que 0, grâce à une condition ternaire. 
  function moins () {
    count < 1 ? setCount(0) : setCount(count - 1);
    } 
  //Celle là réinitialise le nombre de partcipants à 0
  function reset () {
    setCount(0);
  }

  return (

    //un bouton pour incrémenter le compteur
    //un bouton permettant de décrémenter le compteur
    //un bouton pour reset le compteur
    <>
    <p>Nombre de participants : {count}</p>
    
    <button onClick={plus}>Quelqu'un arrive</button>
    <button onClick={moins}>Quelqu'un part</button>
    <button onClick={reset}>Fin de l'évènement</button>
    </>
    
    
   
  )
}

export default App
